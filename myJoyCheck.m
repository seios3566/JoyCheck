function myJoyCheck
% OSで共通のキー配置にする
KbName('UnifyKeyNames');

% いずれのキーも押されていない状態にするため１秒ほど待つ
tic;
while toc < 1; end

% 常に押されるキー情報を取得する
res = JoyCheck(1:7);

% 常に押されている（と誤検知されている）キーがあったら、それを返す
if res(5).isDown || res(7).isDown
  fprintf([
      '押下が検出されたジョイスティックのボタンあるいはキーボードのキーありますが無効化はしていません。\n'...
      'joyCheck() では、KbCheckを使わずにキー入力の検出をしているため、DisableKeysForKbCheck() の設定も反映されません。\n'...
      '代わりに、検出するキーを指定するようにしてください。例：WaitForJoy()\n'
  ]);
  joyName(res) % キーの名前を表示
end

