
% standard usage
res = joyCheck([1,2]);    % GP-01 and GP-02
res = joyCheck(1:4);      % GP-01, GP-02, GP-03 and GP-04


% In case you want to check inputs from 2 connected gamepads, these sum set, these multiply set and keyboard
res = joyCheck([2,4,5,6,7])
% It may return 
% res = 
% 
%   1�~7 struct array with fields:
% 
%     name
%     isDown
%     secs
%     joyCode
%     joyName
%     joyNCode
%     keyCode
%     KbName

% Then, 

res(2).name
% ans =
% 
%     'GP-02'

res(6).isDown
% ans =
% 
%   logical
% 
%    0

res(5).secs
% ans =
% 
%    8.0488e+05

res(4).joyCode
% ans =
% 
%      1     0     0     0     0     0     1     0     0     0     0     0


% To get pressed button names
btn = joyName(res)
% btn =
% 
%   7�~3 cell array
% 
%     {0�~0 double }    {0�~0 double }    {0�~0 double }
%     {'jLeftArrow'}    {'jX'        }    {0�~0 double }
%     {0�~0 double }    {0�~0 double }    {0�~0 double }
%     {'jUpArrow'  }    {'jX'        }    {0�~0 double }
%     {'jUpArrow'  }    {'jLeftArrow'}    {'jX'        }
%     {0�~0 double }    {0�~0 double }    {0�~0 double }
%     {'return'    }    {0�~0 double }    {0�~0 double }
% 

btn(4)
% ans =
% 
%   1�~2 cell array
%   
%     {'jUpArrow'}    {'jX'       }    {0�~0 double }

% Another way to get joyName
joyName(res(2).joyCode)
% ans =
% 
%   1�~2 cell array
%   
%     {'jLeftArrow'}    {'jX'       }

% How many participants pressed a specific button
res(6).joyNCode
% ans =
% 
%      1     0     0     1     0     0     2     0     0     0     0     0

% To see if START button of GP-02 was pressed, 
any(strcmp(btn(2,:),'jStart'))
% or
res(2).joyCode(joyName('jStart')) == 1


% To see if any X button was pressed, 
any(strcmp(btn(5,:),'jX'))
% or
res(5).joyCode(joyName('jX')) == 1

