function result = joyName(arg)
% Usage of this function is similar to that of KbName()
% 
% usage:
%   joyName(1)
%       -> 1x1 cell array   {'jUpArrow'}
% 
% 
%   res = joyCheck(1:4);
%   joyName([0 0 0 0 1 0 0 0 0 1 0 0])
%   joyName(res(2).joyCode)
%       -> 1x2 cell array   {'jA'}    {'jRightButton'}
% 
% 
%   joyName('jA')
%   joyName({'jA'})
%       -> 1x1 double   5
% 
% 
%   joyName({'jX', 'jLeftButton', 'jBack'})
%       -> 1x3 double   [7,9,11]
% 
% 
%   res = joyCheck(1:7);
%   btn = joyName(res)
%       -> 7�~3 cell array
%           {0�~0 double  }    {0�~0 double  }    {0�~0 double }
%           {'jLeftArrow'}    {'jX'        }    {0�~0 double }
%           {0�~0 double  }    {0�~0 double  }    {0�~0 double }
%           {'jUpArrow'  }    {'jX'        }    {0�~0 double }
%           {'jUpArrow'  }    {'jLeftArrow'}    {'jX'       }
%           {0�~0 double  }    {0�~0 double  }    {0�~0 double }
%           {'return'    }    {0�~0 double  }    {0�~0 double }
% 
%   joyName(btn) will return error but,
%   joyName(btn(2,:))
%       -> 1x2 double   [4,7]
% 
% 

    jNames{1} = 'jUpArrow';
    jNames{2} = 'jRightArrow';
    jNames{3} = 'jDownArrow';
    jNames{4} = 'jLeftArrow';
    jNames{5} = 'jA';
    jNames{6} = 'jB';
    jNames{7} = 'jX';
    jNames{8} = 'jY';
    jNames{9} = 'jLeftButton';
    jNames{10} = 'jRightButton';
    jNames{11} = 'jBack';
    jNames{12} = 'jStart';

    narginchk(1, 1);
    if isempty(arg)
        result=[];

    elseif islogical(arg) || (isa(arg,'double') && length(arg)==12) || (isa(arg,'uint8') && length(arg)==12)
        if length(find(arg)) == 12
            result = jNames;
        else
            result = joyName(find(arg));
        end

    elseif isa(arg,'double') || isa(arg,'uint8')
        if length(arg) == 1
            if(arg < 1 || 12 < arg)
                error('Argument exceeded allowable range of 1-12');
            else
                result = jNames(arg);
            end
        else
            for i = 1:length(arg)
                result(i) = joyName(arg(i));

            end
        end

    % input : joyName like, 'jUpArrow'
    % output : joyCode
    elseif ischar(arg)
        result = find(strcmpi(jNames, arg));
        if isempty(result)
            error(['Button name "' arg '" not recognized.']);
        end

    % input : cell array of joyNames
    % output : vector of joyCodes
    elseif isa(arg, 'cell')
        result = [];
        argSize = size(arg);
        if argSize(1) == 1
            for i = 1:length(arg)
                result(i) = joyName(arg{i});
            end
        else
            error('Do not input cell array that is returned by joyName(joyCheck()). Please input 1x* size cell array.');
        end

    % input : struct array of output argument of joyCheck()
    % output : cell array of joyNames and keyCodes
    elseif isstruct(arg)
        empty = 1;
        for i = 1:6
            joyLen = length(find(arg(i).joyCode));
            if empty && ~joyLen
                continue;
            elseif empty && joyLen
                empty = 0;
            end
            result(i, 1:joyLen) = jNames(logical(arg(i).joyCode));
        end
        kbName = KbName(arg(7).keyCode);
        if isempty(kbName)
            if ~isempty(find(arg(7).keyCode, 1))
                result(7,1) = cellstr(['keyCode=' num2str(find(arg(7).keyCode))]);
            elseif ~empty
                result(7,1) = cell(1);
            else
                result = [];
            end
        elseif ischar(kbName)
            result(7,1) = cellstr(kbName);
        else
            kbLen = length(kbName);
            if iscellstr(kbName)
                result(7,1:kbLen) = kbName;
            else
                for j = 1:kbLen
                    if iscellstr(kbName(j))
                        result(7,j) = kbName(j);
                    else
                        pressedKeys = find(arg(7).keyCode);
                        result(7,j) = cellstr(['keyCode=' num2str(pressedKeys(j))]);
                    end
                end
            end
        end
    else
        error('joyName can not handle the supplied argument.');
    end
end
