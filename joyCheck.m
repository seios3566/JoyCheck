function response = joyCheck(respVec)
% joyCheck - is for getting responses from JoySticks(Gamepads).
%
% Syntax: responce = joyCheck(respVec)
%
% Standard Usage:
%       response = joyCheck([1,2,3]);
%
% Input argument:
% 'respVec' - should be  a vector. It is to consist of at most
%   7 values which specify JoySticks or keyboard that you want
%   to get data.
%   For full response, respVec = [1,2,3,4,5,6,7]
%       1-4 mean GP-01 - GP-04, and these number is to bind
%       to calculate following 2 elements (any and all).
%       5 is to get pressed buttons at any JoySticks specified
%       6 is to get pressed buttons at all JoySticks specified
%       7 is to get raw input of keyoard
%   You can omit and specify contents of 'response' by this
%   vector.
%
%   To get any responses from GP-01 and GP-02, input
%       response = joyCheck([1,2,5]);
%   In this case, response(5) is sum set of inputs from GP-01 ~ 
%   GP-02. 
%
%   If you want to get pressed buttons at all JoySticks (that you
%   specify as 'respVec'),
%       response = joyCheck([1,2,6]);
%
%   In case of getting raw input of keyboard and GP-01 ~ GP-04,
%       response = joyCheck([1:4,7])
%
%
% Output:
% 'response' - is a 1x7 struct array. It contains some fields.
%
%   i = 1:4
%   response(i).name = 'GP-0i'              char
%   response(i).isDown = true               logical
%   response(i).secs = 1.3624e+05           double
%   response(i).joyCode = [1,0,0, ...]      1x12 double
%
%   i = 5:6
%   response(5).name = 'any'                char
%   response(6).name = 'all'                char
%       response(5) and response(6) has 'isDown', 'secs' and 'joyCode'
%       fields in the same way as response(1).
%       Additionally response(6) has 'joyNCode' field.
%   response(6).joyNCode = [2,0,1,0, ...]   1x12 double
%       Each value represents the number of JoySticks that were
%       pressed the button.
%
%   i = 7
%   response(7).name = 'keyboard'           char
%   response(7).isDown = false              logical
%   response(7).secs = 1.3624e+05           double
%   response(7).keyCode = [0,0,0, ...]      1x256 double
%       'keyCode' field DOES NOT contain the data
%       from any JoySticks so that this is raw input from
%       keyboards.
%
%
% .joyCode(i) is corresponded to a following button of JoyStick.
%   i   button  joyName(i)
% ----------------------------
%   1   ??      jUpArrow
%   2   ->      jRightArrow
%   3   ??      jDownArrow
%   4   <-      jLeftArrow
%   5   A       jA
%   6   B       jB
%   7   X       jX
%   8   Y       jY
%   9   L       jLeftButton
%   10  R       jRightButton
%   11  BACK    jBack
%   12  START   jStart



% [keyIsDown,secs,keyCode] = KbCheck;
[keyIsDown,secs, keyCode]= Screen('GetMouseHelper', -1);    % faster than KbCheck


narginchk(1, 1);
if ~isnumeric(respVec)
    error('Invalid argument was input! isnumeric(respVec)==true');
else
    if min(respVec) < 1
        error('Invalid argument was input! min(respVec)>=1');
    elseif max(respVec) > 7
        error('Invalid argument was input! max(respVec)<=7');
    end
end

respVecLogical = zeros(1,7);
respVecLogical(respVec) = 1;

if ~any(respVecLogical([1:4, 7]))
    error('Invalid argument was input! Please specify devices (input two or more nuumbers of 1~4) to return response(5) or response(6).');
end
respJoy = find(respVecLogical(1:4));

% initialization
response(7).name = [];
response(7).isDown = [];
response(7).secs = [];
response(7).joyCode = [];
response(7).joyNCode = [];
response(7).keyCode = [];

if respVecLogical(1)
    response(1).name = 'GP-01';
end
if respVecLogical(2)
    response(2).name = 'GP-02';
end
if respVecLogical(3)
    response(3).name = 'GP-03';
end
if respVecLogical(4)
    response(4).name = 'GP-04';
end
bind = sprintf('%d', find(respVecLogical(1:4)));
if respVecLogical(5)
    response(5).name = ['any of ' bind];
end
if respVecLogical(6)
    response(6).name = ['all of ' bind];
end
if respVecLogical(7)
    response(7).name = 'keyboard';
end

if ~keyIsDown
    for i = respVec
        response(i).isDown = false;
        response(i).secs = secs;
        if i ~= 7
            response(i).joyCode = zeros(1,12);
            if i == 6
                response(i).joyNCode = zeros(1,12);
            end
        else
            response(i).keyCode = keyCode;
        end
    end
else
    % Important : Here are mappings from keyCode to joyCode
    code(1,:) = keyCode(148:159);
    code(2,:) = keyCode([193:203,205]);
    code(3,:) = keyCode(206:217);
    code(4,:) = keyCode(228:239);
    if isempty(respJoy)
        code(5,:) = zeros(1,12);
        code(6,:) = zeros(1,12);
    elseif length(respJoy) == 1
        code(5,:) = code(respJoy,:);
        code(6,:) = code(respJoy,:);
    else
        code(5,:) = any(code(respJoy,:));
        code(6,:) = all(code(respJoy,:));
    end
    keyCode([148:159,193:203,205:217,228:239]) = 0;

    for i = respVec
        response(i).secs = secs;
        if i < 7
            response(i).isDown = any(code(i,:));
            if response(i).isDown == 1
                response(i).joyCode = double(code(i,:));
            else
                response(i).joyCode = zeros(1,12);
            end
            if i == 6
                if length(respJoy) == 1
                    response(i).joyNCode = double(code(i,:));
                else
                    response(i).joyNCode = double(sum(code(respJoy,:)));
                end
            end
        else
            response(7).isDown = any(keyCode);
            response(7).keyCode = double(keyCode);
        end
    end
end
end
