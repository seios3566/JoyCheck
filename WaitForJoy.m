function result = WaitForJoy(joyVec,btnName,waitType,releaseCheck,timeout)
% usage:
%    WaitForJoy(1:2, 'jStart');
%       Basic usage. 
% 
%    WaitForJoy(1:3, {'jStart', 'jA'});
%       Wait for anyone of 3 pressing 'jStart' or 'jA' button
% 
%    WaitForJoy(1:2, 'jStart', 'everyone');
%       Wait for each of 2 pressing 'jStart'
% 
%    WaitForJoy(1:2, 'jStart', 'anyone', false);
%       Wait for anyone of 2 pressing 'jStart' without waiting
%       for all button released before checking
% 
%    WaitForJoy(1:2, 'jStart', 'anyone', true, 3);
%       Wait for anyone of 2 pressing 'jStart' for 3 seconds
% 
%
% Input argument:
% 'joyVec' - should be a vector. It is to consist of at most
%   4 values which specify JoySticks that you want to get data.
%   For full response, respVec = [1,2,3,4]
%       1-4 mean GP-01 - GP-04 as the same as joyCheck().
%
% 'btnName' - should be a string or a cell of strings of joyNames
%   like 'jUpArrow', 'jA', 'jY', 'jRightButton', or written in
%   the script of joyName().
%
% 'waitType' - should be 'anyone' or 'everyone' string.
%
% 'releaseCheck' - should be a logical value (true or false).
%
% 'timeout' - should be a positive numeric of timeout in seconds.
% 
%
% Output argument:
% 'result' - is a 1x7 struct array as the same structure as 
%   joyCheck() returns. This is optional, so you have to write
%   an output argument when you want.
%   Following sentense won't return any value even if you omit ';'.
%       WaitForJoy(1:2, 'jStart')
%   If you want the return value, write
%       res = WaitForJoy(1:2, 'jStart');
% 

% MEMO
% 
%
%

% input check
narginchk(2, 5);
if ~isnumeric(joyVec)
    error('Invalid argument was input! isnumeric(joyVec)==true');
else
    if min(joyVec) < 1
        error('Invalid argument was input! min(joyVec)>=1');
    elseif max(joyVec) > 4
        error('Invalid argument was input! max(joyVec)<=4');
    end
end

targetJoyCode = joyName(btnName);

% default arguments
if nargin == 2
    waitType = 'anyone';
    releaseCheck = true;
    timeout = Inf;
elseif nargin == 3
    releaseCheck = true;
    timeout = Inf;
elseif nargin == 4
    timeout = Inf;
end

if ~any(strcmp({'anyone', 'everyone'}, waitType))
    error('Invalid argument was input! waitType : "anyone" or "everyone"')
end

if ~isnumeric(timeout)
    error('Invalid argument was input! isnumeric(timeout)==true');
else
    if timeout <= 0
        error('Invalid argument was input! timeout>0');
    end
end

%
if nargout > 0
    result(7).name = [];
    result(7).isDown = [];
    result(7).secs = [];
    result(7).joyCode = [];
    result(7).joyNCode = [];
    result(7).keyCode = [];
end

% see if specified button is released
if releaseCheck
    while 1
        res = joyCheck([joyVec,5]);
        if ~res(5).joyCode(targetJoyCode)
            isBtnOn = zeros(max(joyVec),12);
            break;
        end
    end
end

t = tic;
while 1
    res = joyCheck([joyVec,5,7]);
    if any(res(5).joyCode(targetJoyCode))
        for i = joyVec
            if any(res(i).joyCode(targetJoyCode)) && ~any(isBtnOn(i,targetJoyCode))
                isBtnOn(i,:) = res(i).joyCode;
                if nargout > 0
                    result(i).name = res(i).name;
                    result(i).isDown = res(i).isDown;
                    result(i).secs = res(i).secs;
                    result(i).joyCode = res(i).joyCode;
                end
            end
        end
        if strcmp('anyone', waitType) && any(any(isBtnOn(joyVec,targetJoyCode)))
            break;
        end
        if strcmp('everyone', waitType) && all(any(isBtnOn(joyVec,targetJoyCode), 2))
            break;
        end
    elseif res(7).keyCode(KbName('escape'))
        msg = 'Error: Escape key is hit.';
        error(msg);
    elseif timeout ~= Inf && toc(t) > timeout
        break;
    end
end
end
